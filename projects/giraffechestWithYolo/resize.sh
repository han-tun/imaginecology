#!/bin/bash

folder="/home/gdussert/Documents/images" #folder containing the images
dest_folder="/home/gdussert/Documents/images_resized/" #folder where the resized image will be saved
resize="1024x1024" #maximum resolution of the resized images

mkdir $dest_folder

for ext in jpg JPG png PNG
do
	for file in ${folder}/*.$ext
	do
		[ -e "$file" ] || continue
		echo "convert $file"
		name=`basename $file .$ext`
		convert $file -resize $resize $dest_folder/${name}.jpg
	done
done
