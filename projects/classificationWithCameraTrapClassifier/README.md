# Classification of camera trap images with Camera Trap Classifier

Camera Trap Classifier (CTC) is a CNN that can classify camera trap images. For a each image, it gives the name of the species on the image, the number of animals and other binary predictions "standing", "resting", "moving", "eating", "interacting" and "young presence".  

The model can predict **85 different species** and is trained on 1.8 million of images, mainly from Tanzania (Serengeti National Part) and South Africa.  The list of species and the performance of the model in different locations can be found [here](https://github.com/marco-willi/camera-trap-classifier/blob/master/docs/trained_models.md#evaluation-results) in the Evaluation Results. 

## Installation

* Python 3 and [Tensorflow](https://www.tensorflow.org/install) (see the [GPU Guide](https://www.tensorflow.org/install/gpu) for GPU support) must be installed.

* Clone or download this repositery : [Camera Trap Classifier](https://github.com/marco-willi/camera-trap-classifier)

* Go inside `camera-trap-classifier` directory.

  Here two possibilities :

  * You have Tensorflow 1.12, it's all good and you can run the setup by doing `python3 setup.py install`

  * You have a more recent version of Tensorflow: 

    Edit `setup.py` and remove the lines  15,16 :

    ```
    'tf': ['tensorflow==1.12'],
    'tf-gpu': ['tensorflow-gpu==1.12']
    ```

    Doing that you won't be able to train the model but the detection part will still work.

    Now you can run the setup : `python3 setup.py install`

* Go inside `camera-trap-classifier/camera_trap_classifier/` directory and run the tests :

  ```
  python -m unittest discover test/data
  python -m unittest discover test/predicting
  ```

  If you get the error : `ImportError: No module named camera_trap_classifier.predicting.processor`, you need to add `camera-trap-classifier/` to your `PYTHONPATH` :

  ``` bash
  export PYTHONPATH="$PYTHONPATH:[PATH_1]"
  ```

  with [PATH_1] the absolute path to `camera-trap-classifier/` (on Linux, add this line into your `.bashrc` file if you don't want to do it every time you use the module). 

* Download the model : [Xception_v1.zip](https://s3.msi.umn.edu/snapshotsafari/models/species/Xception_v1.zip) and extract it in `camera-trap-classifier/`.

## Detection

Download these four [images](ftp://pbil.univ-lyon1.fr/pub/datasets/imaginecology/ctc/images/) and put them in an `images/` directory.

You need only one command to run the detection : 

```bash
ctc.predict -image_dir [PATH_1] -results_file [PATH_2] -model_path [PATH_3]/best_model.hdf5 -class_mapping_json [PATH_3]/label_mappings.json -pre_processing_json [PATH_3]/image_processing.json 
```

With : 

* [PATH_1] the path to `images/`
* [PATH_2] the name of the output file (for instance `output.csv`)
* [PATH_3] the path to the directory where you have extracted `Xception_v1.zip`

**NB:** If you get `ctc.predict: command not found` , you need to add the location of the script to your `PATH`, it was written in the terminal after running `python3 setup.py install` during the installation step. 

In general something like:  `export PATH=[YOUR_HOME]]/.local/bin:$PATH`.



## Results 

Our results are in [output.csv](output.csv), open the file and you should have something like this :

<center>
<img src="../../images/ctc_csv.png " width="600">
</center>

**Warning:** in our various experiments, **species predictions were not always correct !** Model performance may vary a lot depending on the way pictures were taken (for instance the distance from the subject) and on the species you are interested in. 
Be also cautious with the others predictions : count, standing, resting, etc. 