# Detection of animals on camera trap images with RetinaNet

by Gaspard Dussert & Vincent Miele  ([CNRS/LBBE](https://lbbe.univ-lyon1.fr/))

<div style="text-align: justify"> 

[RetinaNet](../../papers/arxiv-lin2018_focal_loss.pdf) is a single-stage object detector : for each known object on an image it can predict a **class**, a **bounding box** (a square containing the object) and a **confidence score**. 


<center>
<img src="../../images/retinanet_reconyx2.png" width="600">
</center>

**NB:**  this tutorial has been tested on Linux and MacOS with Catalina (version 10.15.4) only

## 0. Dataset used in this tutorial

In this tutorial we are going to work on the public [Reconyx Camera Trap](../#reconyx-camera-trap) dataset. There are 946 images for 20 different species. 

Since the training dataset is quite small, it is impossible to learn a model directly. The trick, aka [transfer learning](https://machinelearningmastery.com/transfer-learning-for-deep-learning/) with CNN, consists in reusing a pre-trained model as the starting point for a model on a particular task of interest.


<center>
<img src="../../images/transferlearning.jpeg" width="400">
</center>

In this tutorial the model has been pre-trained on the COCO dataset, the most famous one for object detection. In the dataset there are 80 object categories, including some animal species. Thanks to this, the model has already learned some general features that will help to detect one of the 20 species of interest.

## 1. Setting the training and validation set

### 1.1. (Optional) Resizing the images and annotating the images 

This part is optional because we already prepared the dataset (see 1.2.). However, if you want to train the model on your own dataset, you must do it. 

To train the model, you need to annotate the images with the coordinates of every bounding boxes around the object you are interested in. 
But as these coordinates are dependent on the image size, it is mandatory to resize your images before annotating them!!

> Why do we resize them ? 

Because when an image is too large, it will be automatically resized by RetinaNet during the training step every time it is used : it will increase the computing time!

#### Resizing the images

Here we are going to resize the images to a maximum size of 1024x1024. 

To do this on Linux, you can use/adapt the script [```resize.sh```](resize.sh)  (the script uses [ImageMagick](https://imagemagick.org/)) :
* Edit the script and set `folder` to your path to the images and `dest_folder` to the path where the converted images will be saved, for example `images_resized`.
* Run the script with 
```
./resize.sh
```

or alternatively use the R script  [```resize.R```](resize.R) developed by [@oag.gimenez](https://gitlab.com/oag.gimenez)

<a name="annots"></a>

#### Annotating the images

RetinaNet expects 3 different files : `train.csv`, `val.csv` and `class.csv`.

Each row of `train.csv` and  `val.csv` represents a bounding box in the following format : 

```
path/to/image.jpg,xmin,ymin,xmax,ymax,class_name
```

And each row of `class.csv` contains a class name and an integer ID in the format:

```
class_name,id
```

For an example, see the files  [train.csv](train.csv) and [class.csv](class.csv). 

Importantly, **images without any boxes** can be used during the training as negative examples to improve the performance (this is highly recommended). The annotation must look like :

```
path/to/image.jpg,,,,,
```

**NB:** To annotate your own dataset you can use for example [VGG Image Annotator](http://www.robots.ox.ac.uk/~vgg/software/via/) to draw the bounding boxes or [MegaDetector](../detectionWithMegaDetector) to generate them automatically. 

### 1.2. Download the images and annotations

The resized images are available [here](ftp://pbil.univ-lyon1.fr/pub/datasets/imaginecology/retinanet/images/), download them in the `images/` folder in the main directory (the one with `class.csv`, `val.csv`, etc.) 

To download the images easily on linux, create the `images/` directory and run this command from it :
```bash
wget -r -nd -np ftp://pbil.univ-lyon1.fr/pub/datasets/imaginecology/retinanet/images/
```

The associated annotations are also available in our files [train.csv](https://gitlab.com/ecostat/imaginecology/blob/master/projects/cameraTrapDetectionWithRetinanet/train.csv), [val.csv]( https://gitlab.com/ecostat/imaginecology/blob/master/projects/cameraTrapDetectionWithRetinanet/val.csv) and [class.csv](https://gitlab.com/ecostat/imaginecology/blob/master/projects/cameraTrapDetectionWithRetinanet/class.csv) in the current GitLab directory.


## 2. Installation of RetinaNet

Python 3, [Tensorflow](https://www.tensorflow.org/install) (see the [GPU Guide](https://www.tensorflow.org/install/gpu) for GPU support) and [Keras](https://keras.io/#installation) must be installed on your machine. 

* Clone or download this repository : [keras-retinanet](https://github.com/fizyr/keras-retinanet)

* From `keras-retinanet/`, run : 

  ``` 
  pip install numpy --user 
  ```

  and 

  ```
  pip install . --user
  ```

Warning: by default, `pip` must be `pip3` if you are using `python3` !

Finally, download the pre-trained weights : [resnet50_coco_best_v2.1.0.h5](https://github.com/fizyr/keras-retinanet/releases/download/0.5.1/resnet50_coco_best_v2.1.0.h5) and also move it to the main directory.

##  3. Verifying the annotations

RetinaNet includes a script to verify the your annotations are correctly made before starting the training. 

From the main directory of the project, run this command :

```bash
retinanet-debug --annotations csv train.csv class.csv
```

The script display one image and the bounding boxes that have been annotated. Use the keyboard arrows to change of image and `Esc` to stop. 

**NB :** If the script `retinanet-debug` is not found, use python to call `debug.py` with its absolute path: 
```bash
python3 .../keras-retinanet/keras_retinanet/bin/debug.py --annotations csv train.csv class.csv
```

## 4. Starting the training

To start the training with the defaults parameters just run :

```bash
retinanet-train --weights resnet50_coco_best_v2.1.0.h5 --snapshot-path snapshots/ csv train.csv class.csv --val-annotations val.csv
```

`--snapshot-path` is the the path to the directory in which the model will be saved after each epoch of training. 

Other parameters should be added to control the training : 

`--epochs X` : to run the training during X epochs. (Default : 50 epochs)

`--steps X` : each epoch contains X steps (batches). (Default : 10000 steps)

`--batch-size X` : each batch contains X images.  (Default : 1 image)

`--tensorboard-dir [PATH_1]` : folder in which `Tensorboard` logs are saved. (Default : `./logs/`)

`--compute-val-loss` : compute the validation loss after each epoch. (Default : False)

**NB:** Here is a [glossary](../../doc/README.md) of the deep learning vocabulary. 

Here, we propose to train the model in 10 epochs, 2000 steps per epoch and 2 images per batch. 

```bash
retinanet-train --weights resnet50_coco_best_v2.1.0.h5 --snapshot-path snapshots/ --batch-size 2 --steps 2000 --epochs 10 --compute-val-loss csv train.csv class.csv --val-annotations val.csv
```

(about 2h30 to do the full training with a Titan X GPU)

**NB 2:** Once again if `retinanet-train` is not found, replace it by `python3 .../keras-retinanet/keras_retinanet/bin/train.py`

**NB 3:** Other parameters can be changed, see their definition in [train.py](https://github.com/fizyr/keras-retinanet/blob/master/keras_retinanet/bin/train.py#L372) 

## 5. Choosing the best weights

When the training is performed, you have **a weight file for every epoch** but you should not necessary use the last one since the model can overfitted. 
To find the best weights, you must look at the validation loss graph and look at which epoch the loss is at its minimum. 

<center>
<img src="../../images/overfit.png" width="300">
</center>

To find this minimum you can either look at the output of RetinaNet which prints the validation mAP (mean Average Precision) at the end of each epoch or use [Tensorboard](https://www.tensorflow.org/tensorboard). Tensorboard is a package that comes with the Tensorflow package, very easy to use!

To use Tensorboard, use the command ```tensorboard --logdir=[PATH_1]``` where ```[PATH_1]``` is the absolute path to ```logs/``` directory. It will output a link like this ```http://[name_of_the_machine]:6006```, just copy/paste it in your browser to access Tensorboard.

<center>
<img src="../../images/tensorboard_reconyx.png" width="800">
</center>

Here we are interested by the validation loss and choose epoch 4.



**NB:** If you want to access it from an other computer on the same network, use ```http://[ip_address_of_the_machine]:6006```

**NB2:** If the command ```tensorboard``` is not found, find where it is installed with ```pip3 show tensorboard```.

## 6. Detecting the animals on a test set

If you don't have a GPU to do the training, here is a link to the [trained model](ftp://pbil.univ-lyon1.fr/pub/datasets/imaginecology/retinanet/retinanet_tutorial_best_weights.h5). Move it to the main folder. 

It is time to run the detector on a test set: the images also come from Reconyx Camera Trap but were not used during the training.

RetinaNet has a script to evaluate the performance and generate the images with the predicted bounding boxes. To use it, a `test.csv` file with the filenames (and optionally the annotations) is required. We propose here a `test.csv` file associated to the test images available [here](ftp://pbil.univ-lyon1.fr/pub/datasets/imaginecology/retinanet/test/). Put them in the `test/` folder. 

**NB :** If you want to evaluate the model on a dataset without annotations just fill the `.csv` file with filenames and empty annotations (See [annotating the image](#annots) at the beginning of the tutorial). 

To start the evaluation, just run this command from the main directory : 

``` 
retinanet-evaluate --convert-model --save-path test_pred/ --score-threshold 0.5 csv test.csv class.csv retinanet_tutorial_best_weights.h5
```

`--save-path test_pred/` : `test_pred/` is the folder in which the prediction are saved. Remove it to only compute the performance without saving the images. 

`--score-threshold X` : X is the threshold of confidence for a prediction.

It will print the average precision for each class and the mAP of the test dataset. Here we have a mean average precision of 71%.

On the generated images, **the ground truth bounding box is in green if there is one** and the predicted one is in another color. 

<center>
<img src="../../images/retinanet_reconyx.png" width="600">
</center>



To get the coordinates of the boxes, writing a piece of Python code is necessary!! 

See an example in [detect2txt.py](detect2txt.py): the detection is made using ``model.predict_on_batch()`` of the ``keras_retinanet`` module. It returns text information including filename, label, score (confidence) and box coordinates.

Also, [detect2.py](detect.py): same principle, to visualize the prediction boxes.  

## Warning 

This **tutorial** is only a **toy-example**. We do not guarantee the detection performance on images from  another dataset : the dataset used for the training is far too small and the detector will very likely not generalize well!!  To achieve good performance, we recommend at least 200 images per species under interest, with as much diversity (position, lightning,...) as possible. 

