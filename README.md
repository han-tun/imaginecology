# IMAGINECOLOGY

# Image, Ecology & Deep learning

**A curated list of deep learning resources for computer vision in ecology !**

<center>
<img src="images/bandeau.png " height="150">
</center>

Sponsored by the [EcoStat GdR](https://sites.google.com/site/gdrecostat/ "GdR EcoStat Homepage") in [CNRS](http://www.cnrs.fr "CNRS Homepage") with the contribution of [Vincent Miele](https://gitlab.com/vmiele), [Gaspard Dussert](https://gitlab.com/Gasp34), Simon Chamaillé-Jammes, Christophe Bonenfant, Olivier Gimenez and other colleagues@ecostat

<center>
<img src="images/ecostat.jpg" height="150">
<img src="images/cnrs.jpg" height="150"><br>
</center>

