Le GdR EcoStat organise un **workshop "imaginecology"** sur le thème du 
"Deep Learning pour le traitement d'image (ou du son) en écologie".

Ce sera les **16 et 17 novembre** 2020.

Plus d'informations ici :  [https://imaginecology.sciencesconf.org/](https://imaginecology.sciencesconf.org/)

*Sakina-Dorothée Ayata, Olivier Dézerald, Stéphane Dray, Guglielmo Fernandez Garcia, Olivier Gimenez, François Martignac, Vincent Miele et Julien Renoult*