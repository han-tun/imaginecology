#### [The penguin dataset](https://www.robots.ox.ac.uk/~vgg/data/penguins/)
- X images (28GB), 40 different locations
- Annotations : X-Y coordinates on each penguin (json or MAT)

#### [Oregon Wildlife image collection](https://www.kaggle.com/virtualdvid/oregon-wildlife)
- 14013 images, 20 species, (4,36 GB)
- Annotations : name of the class (images sorted by folder)
- 5 classes already preprocessed with GapCV (?)

#### [Caltech camera Trap dataset](https://beerys.github.io/CaltechCameraTraps/)
- 243,187 (104 GB) images with class annotation 
- 57,864 images with bounding box annotation
- Annotations in [COCO format ](https://github.com/Microsoft/CameraTraps/blob/master/data_management/README.md#coco-cameratraps-format) (json)

#### [North American Camera Trap Images (NACTI)](http://lila.science/datasets/nacti)
- 3.7M images (1,3 TB)
- labels for 28 animal categories, primarily at the species level
- Annotations in [COCO format ](https://github.com/Microsoft/CameraTraps/blob/master/data_management/README.md#coco-cameratraps-format) (json)
- Along with [Tabak et al, MEE 2018](papers/mee-tabak2018_cameratrap_tensorflow.pdf)

#### [Snapshot Serengeti](http://lila.science/datasets/snapshot-serengeti)
- 6.7M images
- Labels for 55 animal categories, primarily at the species level
- 78 000 images with bounding box
- Annotations in [COCO format ](https://github.com/Microsoft/CameraTraps/blob/master/data_management/README.md#coco-cameratraps-format) (json)
- Along with [Swanson et al, Sci. Data 2015](papers/scidata-swanson2015_snapshot_serengeti.pdf)

#### [Wildlife Image and Localization Dataset (WILD)](http://lila.science/otherdatasets) 
- A new ground-truthed dataset for the tasks presented in this [paper](papers/proc-parham2018_deeplearn_animal.pdf), called WILD; WILD is comprised of photographs taken by biologists, wildlife rangers, citizen scientists [13], and conservationists.
- 5,784 images, 28 species (1,4 GB)
- Annotations : Bounding box in Pascal VOC format
- Link to download not working

#### [The Aerial Elephant Dataset](https://zenodo.org/record/3234780#.XX-ENaaxXmg)
- From this [paper]((http://openaccess.thecvf.com/content_CVPRW_2019/papers/DOAI/Naude_The_Aerial_Elephant_Dataset_A_New_Public_Benchmark_for_Aerial_CVPRW_2019_paper.pdf))
- 2101 images (16 GB) containing a total of 15 511 elephants. It is split into training and test subsets with 1649 images containing 12455 elephants in the training set and 452 images containing 3056 elephants in the test set.
- Annotations : ?

#### [iWildCam2019](https://www.kaggle.com/c/iwildcam-2019-fgvc6/overview) 
- Kaggle competition (finished) with data from CaltechCameraTraps, iNaturalist 2017/2018, and simulated data generated from Microsoft AirSim. 
- 196,157 images (43 GB) from 138 different locations in Southern California, 14 classes.
- Annotations : class (.csv)

#### [LILA](http://lila.science/datasets/)
- Many labeled camera trap dataset are hosted and available on this website. 
- Also look at [others](http://lila.science/otherdatasets) for more datasets.

#### [Reconyx Camera Trap](https://dataverse.scholarsportal.info/dataset.xhtml?persistentId=doi:10.5683/SP/TPB5ID)
- Used in [Schnieder et al, arxiv 2019](papers/arxiv-schneider2018_camera_trap.pdf)
- 7,193 camera trap images from two locations in Panama and the Netherlands, capturing colour images during the day, and gray-scale at night.
- Only a subset of 946 images include labeled bounding box coordinates

#### [FishNet](https://www.fishnet.ai/description)

* ~35K images taken from monitoring cameras on fishing boats
* 24 object classes (fish species and human)
* Annotations : bounding box in .csv format.


#### [EcoTaxa](https://ecotaxa.obs-vlfr.fr)

EcoTaxa is a web application dedicated to the visual exploration and the taxonomic annotation of images that illustrate the beauty of planktonic biodiversity.
